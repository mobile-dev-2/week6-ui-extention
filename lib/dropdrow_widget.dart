import 'package:flutter/material.dart';

class DropDownWigget extends StatefulWidget {
  DropDownWigget({Key? key}) : super(key: key);

  @override
  _DropDownWiggetState createState() => _DropDownWiggetState();
}

class _DropDownWiggetState extends State<DropDownWigget> {
  String vacine = "-";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            child: Row(
              children: [
                Text('Vaccine'),
                Expanded(child: Container()),
                DropdownButton(
                  items: [
                    DropdownMenuItem(
                      child: Text('-'),
                      value: '-',
                    ),
                    DropdownMenuItem(
                      child: Text('Pfizer'),
                      value: 'Pfizer',
                    ),
                    DropdownMenuItem(
                      child: Text('Johnson & Johnson'),
                      value: 'Johnson & Johnson',
                    ),
                    DropdownMenuItem(
                      child: Text('Sputnik V'),
                      value: 'Sputnik V',
                    ),
                    DropdownMenuItem(
                      child: Text('AztraZeneca'),
                      value: 'AztraZeneca',
                    ),
                  ],
                  value: vacine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vacine = newValue!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
            child: Text(vacine, style: TextStyle(fontSize: 20.0)),
          )
        ],
      ),
    );
  }
}
