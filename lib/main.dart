import 'package:flutter/material.dart';
import 'package:ui_extention/checkboboxtile_widget.dart';
import 'package:ui_extention/checkbox_widget.dart';
import 'package:ui_extention/dropdrow_widget.dart';
import 'package:ui_extention/radio_widget.dart';

void main() {
  runApp(MaterialApp(title: "ui Extention", home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('UI Extention')),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text('UI Menu'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('CheckBox'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckBoxWidget()));
              },
            ),
            ListTile(
              title: Text('CheckBoxTile'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxTileWidget()));
              },
            ),
            ListTile(
              title: Text('DropDown'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropDownWigget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            )
          ],
        ),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('CheckBox'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CheckBoxWidget()));
            },
          ),
          ListTile(
            title: Text('CheckBoxTile'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CheckBoxTileWidget()));
            },
          ),
          ListTile(
            title: Text('DropDown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropDownWigget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RadioWidget()));
            },
          )
        ],
      ),
    );
  }
}
